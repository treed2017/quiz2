q = pickAFile()
picture = makePicture(q)
def aFunction(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)
  
  for x in range(0,xmax):
    for y in range(0,ymax):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      r = getRed(pixel)
      g = getGreen(pixel)
      newVal = ((r+g+b) / 3) + 75
      r = g = b = newVal
      color = makeColor(r,g,b)
      setColor(pixel, color)
  repaint(picture)
  show(picture)
  
aFunction(picture)